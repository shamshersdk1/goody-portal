<?php
function date_to_ddmmyyyy($date)
{
    if (!empty($date)) {
        return date("d-m-Y", strtotime($date));
    } else {
        return null;
    }
}
function datetime_to_ddmmyyyy($date)
{
    if (!empty($date)) {
        return date("d-m-Y H:i:s", strtotime($date));
    } else {
        return null;
    }
}
function date_to_yyyymmdd($date)
{
    if (!empty($date)) {
        return date("Y-m-d", strtotime($date));
    } else {
        return null;
    }
}

function date_in_view($date)
{
    if (!empty($date)) {
        return date("D, jS M Y", strtotime($date));
    } else {
        return null;
    }
}
function datemonth_in_view($date)
{
    if (!empty($date)) {
        return date("jS M Y", strtotime($date));
    } else {
        return null;
    }
}
function datetime_in_view($date)
{
    if (!empty($date)) {
        // return date("jS M y \a\\t g:i a", strtotime('+5 hours +30 minutes', strtotime($date)));
        return date("jS M y \a\\t g:i a", strtotime($date));
    } else {
        return null;
    }
}
function nameFromCode($code)
{
    $newCode = str_replace('_', ' ', $code);
    return ucwords($newCode);
}
function nameFromTDS($code)
{
    $newCode = str_replace('-', ' ', $code);
    return ucwords($newCode);
}
function custom_round($number)
{
    return round($number, 2);
}
function custom_format($number)
{
    if ($number == 0) {
        return 0;
    }

    $negativeFlag = 0;

    if ($number < 0) {
        $number = (-1) * $number;
        $negativeFlag = 1;
    }
    $amount = round($number, 2);
    $num = floor($amount);
    $fraction = round($amount - $num, 2);

    $explrestunits = "";
    if (strlen($num) > 3) {
        $lastthree = substr($num, strlen($num) - 3, strlen($num));
        $restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
        $restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
        $expunit = str_split($restunits, 2);
        for ($i = 0; $i < sizeof($expunit); $i++) {
            // creates each of the 2's group and adds a comma to the end
            if ($i == 0) {
                $explrestunits .= (int) $expunit[$i] . ","; // if is first value , convert into integer
            } else {
                $explrestunits .= $expunit[$i] . ",";
            }
        }
        $thecash = $explrestunits . $lastthree;
    } else {
        $thecash = $num;
    }

    $frac = substr($fraction, 1);

    if ($negativeFlag == 0) {
        return $thecash . $frac;
    } else {
        return '-' . $thecash . $frac;
    }
}

function custom_money($number)
{
    if ($number == 0) {
        return 0;
    }

    $negativeFlag = 0;

    if ($number < 0) {
        $number = (-1) * $number;
        $negativeFlag = 1;
    }
    $amount = round($number, 2);
    $num = floor($amount);
    $fraction = round($amount - $num, 2);

    $explrestunits = "";
    if (strlen($num) > 3) {
        $lastthree = substr($num, strlen($num) - 3, strlen($num));
        $restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
        $restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
        $expunit = str_split($restunits, 2);
        for ($i = 0; $i < sizeof($expunit); $i++) {
            // creates each of the 2's group and adds a comma to the end
            if ($i == 0) {
                $explrestunits .= (int) $expunit[$i] . ","; // if is first value , convert into integer
            } else {
                $explrestunits .= $expunit[$i] . ",";
            }
        }
        $thecash = $explrestunits . $lastthree;
    } else {
        $thecash = $num;
    }

    $frac = substr($fraction, 1);

    if ($negativeFlag == 0) {
        return '&#8377;' . $thecash . $frac;
    } else {
        return '&#8377;' . '-' . $thecash . $frac;
    }
}
function month_year_in_view($date)
{
    if (!empty($date)) {
        return date("M, Y", strtotime('+5 hours +30 minutes', strtotime($date)));
    } else {
        return null;
    }
}
function year_in_view($date)
{
    if (!empty($date)) {
        return date("Y", strtotime('+5 hours +30 minutes', strtotime($date)));
    } else {
        return null;
    }
}
function date_month_in_view($date)
{
    if (!empty($date)) {
        return date("j M", strtotime('+5 hours +30 minutes', strtotime($date)));
    } else {
        return null;
    }
}
