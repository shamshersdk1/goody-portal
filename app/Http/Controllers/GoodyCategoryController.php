<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\GoodyCategory;
use Illuminate\Http\Request;

class GoodyCategoryController extends Controller
{

    public function index()
    {
        $goodyCategories = GoodyCategory::all();
        return view('pages.goody-category.index',compact('goodyCategories'));
    }

    public function store()
    {
        $giftCatObj = GoodyCategory::create(request()->all());
        if ($giftCatObj->isInvalid()) {
            return redirect()->back()->withErrors($giftCatObj->getErrors())->withInput();
        }
        return redirect()->back()->with('message', '#'.$giftCatObj->id.' Item with name "'.$giftCatObj->name.'" has been created successfully!');
    }

    public function destroy(GoodyCategory $goodyCategory)
    {
        $goodyCategory->delete();
        return redirect()->back()->with('message', 'Successfully Deleted');
    }
}
