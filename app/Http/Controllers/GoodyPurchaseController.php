<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{GoodyCategory, GoodyPurchase, User, GoodyItem, GoodyTransaction };
use App\Services\FileService;
use Auth;
use DB;


class GoodyPurchaseController extends Controller
{
    public function index()
    {
        $goodyCategoryId = request('goody_category_id');
        $giftPurchase = GoodyPurchase::with('user')->orderBy('id','DESC')->get();
        $gifts = GoodyItem::get();
        if(!is_null(request('goody_category_id')))
            $gifts = GoodyItem::where('category_id', request('goody_category_id'))->get();
        $categories = GoodyCategory::all();
        return view('pages.goody-purchase.index',compact('giftPurchase','gifts','categories','goodyCategoryId'));
    }

   
    public function store(Request $request)
    {
        $data = $request->all();   
        DB::beginTransaction();
        $giftPurchaseObj = GoodyPurchase::create(['user_id'=> Auth::id(), 'goody_item_id' => $data['goody_item_id'], 'quantity' => $data['quantity'],'note' => $data['note'], 'total_amount' => $data['total_amount'],'invoice_number' =>$data['invoice_number'],'purchase_order' =>$data['purchase_order']]);
        if ($giftPurchaseObj->isInvalid()) {
            return redirect('goody-purchase')->withErrors($giftPurchaseObj->getErrors())->withInput();
        }
        if(isset($data['goody_purchase_attachments']) && count($data['goody_purchase_attachments'])>0){
            foreach($data['goody_purchase_attachments'] as $attachment)
            {
                $response = FileService::uploadFile($attachment,$giftPurchaseObj->id,'App\Models\GoodyPurchase');
                if ($response['status']) {
                    DB::rollBack();
                    return redirect('goody-purchase')->withErrors($response['message'])->withInput();
                }
            }
        }
        DB::commit();
        return redirect('goody-purchase')->with('message', '"'.$giftPurchaseObj->goodyItem->name.'" stock entry ('.$giftPurchaseObj->quantity.' qty) has been added successfully!');
    }

    public function show($id)
    {
        $giftPurchase = GoodyPurchase::find($id);
        $gifts = GoodyItem::get();
        $readonly = false;
        if (!$giftPurchase) {
            return redirect('/goody-purchase')->withErrors('Goody Purchase not found')->withInput();
        }
        if($giftPurchase->is_approved){
            $readonly = true;
        }
        return view('pages.goody-purchase.edit', compact('giftPurchase','gifts','readonly'));
    }

    public function updateApproval($id)
    {
        $goodyPurchase = GoodyPurchase::where('id', $id)->update(['is_approved' => true]);
        $giftPurchaseObj = GoodyPurchase::find($id);

        if($giftPurchaseObj->is_approved && $giftPurchaseObj->quantity){
          $transactionObj = GoodyTransaction::create(['user_id' => Auth::id(), 'goody_item_id' => $giftPurchaseObj->goody_item_id, 'quantity' => $giftPurchaseObj->quantity,'reference_type' => 'App\Models\GoodyPurchase','reference_id' => $giftPurchaseObj->id]);
            if ($transactionObj->isInvalid()) {
                return redirect()->back()->withErrors($transactionObj->getErrors())->withInput();
            }
        }

        return redirect()->back()->with('message','"'.$giftPurchaseObj->goodyItem->name.'" stock entry ('.$giftPurchaseObj->quantity.' qty) approved successfully');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $goodyPurchaseObj = GoodyPurchase::updateOrCreate(['id' => $id], ['user_id'=> Auth::id(), 'goody_item_id' => $data['goody_item_id'], 'quantity' => $data['quantity'],'note' => $data['note'], 'total_amount' => $data['total_amount'],'invoice_number' =>$data['invoice_number'],'purchase_order' =>$data['purchase_order']]);
        if ($goodyPurchaseObj->isInvalid()) {
            return redirect()->back()->withErrors($goodyPurchaseObj->getErrors())->withInput();
        }
        return redirect()->back()->with('message', '# '.$goodyPurchaseObj->id.' Item with name "'.$goodyPurchaseObj->goodyItem->name.'" has been updated successfully!');
    }

    public function destroy($id)
    {
        $giftObj = GoodyPurchase::find($id);
        if ($giftObj && $giftObj->delete()) {
            $transactionObj = GoodyTransaction::create(['user_id' => Auth::id(),'goody_item_id' => $giftObj->goody_item_id, 'quantity' => - abs($giftObj->quantity), 'reference_type' => 'App\Models\GoodyPurchase','reference_id' => $giftObj->id]);
            return redirect('/goody-purchase')->with('message', '# '.$giftObj->id.' Item with name "'.$giftObj->name.'" has been deleted successfully!');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
