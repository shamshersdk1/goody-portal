<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{File, GoodyCategory, GoodyItem, GoodyPurchase, GoodyTransaction};
use App\Services\FileService;
use DB;

class GoodyItemController extends Controller
{
    
    public function index()
    {
        $categories = GoodyCategory::all();
        $gifts = GoodyItem::orderBy('id','DESC')->get();
        $otherGifts = GoodyItem::where('category_id','0')->get();
        
        $data =[];
        foreach($gifts as $gift) {
            $data[$gift->id] = GoodyTransaction::where('goody_item_id',$gift->id)->sum('quantity');
        }
        return view('pages.goody-item.index',compact('gifts','categories','data','otherGifts'));
    }

    public function create()
    {
        $categories = GoodyCategory::all();
        return view('pages.goody-item.add',compact('categories'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $giftObj = GoodyItem::create(request()->all());
        $response = FileService::uploadFile($request->gift_image,$giftObj->id,'App\Models\GoodyItem');
        if ($giftObj->isInvalid() || $response['status']) {
            DB::rollBack();
            return redirect('goody-items')->withErrors($giftObj->getErrors().$response['message'])->withInput();
        }
        DB::commit();
        return redirect('goody-items')->with('message', '#'.$giftObj->id.' Item with name "'.$giftObj->name.'" has been created successfully!');
    }

   
    public function show($id)
    {
        $categories = GoodyCategory::all();

        $gift = GoodyItem::with("image")->find($id);
        if (!$gift) {
            return redirect('/goody-items')->withErrors('Appraisal type not found!')->withInput();
        }
        return view('pages.goody-item.edit', compact('gift','categories'));
    }

    
    public function edit($id)
    {
        
    }
    
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $giftObj = GoodyItem::updateOrCreate(['id' => $id], request()->all());
        if ($giftObj->isInvalid()) {
            DB::rollBack();
            return redirect('/goody-items/'.$giftObj->id)->withErrors($giftObj->getErrors())->withInput();
        }
        if($request->gift_image)
        {
            $response = FileService::updateUploadFile($request->gift_image,$giftObj->id,'App\Models\GoodyItem');
            if ($response['status']) {
                DB::rollBack();
                return redirect('/goody-items/'.$giftObj->id)->withErrors($response['message'])->withInput();
            }
        }
       DB::commit();
        return redirect('/goody-items')->with('message', '# '.$giftObj->id.' Item with name "'.$giftObj->name.'" has been updated successfully!');
    }

    
    public function destroy($id)
    {
        $giftObj = GoodyItem::find($id);
        $goodyPurchase = GoodyPurchase::where('goody_item_id',$id)->get();
        
        if(isset($goodyPurchase) && count($goodyPurchase) > 0){
            return redirect()->back()->withErrors('Can not delete "'.$giftObj->name.'" already exist in purchase record.');
        }
        if ($giftObj && $giftObj->delete()) {
            File::where('reference_type','App\Models\GoodyItem')->where('reference_id', $giftObj->id)->delete();
            return redirect('/goody-items')->with('message', '# '.$giftObj->id.' Item with name "'.$giftObj->name.'" has been deleted successfully!');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
