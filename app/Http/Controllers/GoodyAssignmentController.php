<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\{GoodyCategory, GoodyTransaction, GoodyItem, GoodyPerson};
use Auth;


class GoodyAssignmentController extends Controller
{
    public function index() {
        $categories = GoodyCategory::all();
        $items = GoodyItem::all();
        $goodyPersons = GoodyPerson::all();
        $users = User::where('is_active' , 1)->get();
        $goodyItemId = request('goody_item_id');
        $goodyCategoryId = request('category_id');
        return view('pages.goody-assign.index', compact('items' ,'users', 'goodyPersons','goodyItemId','goodyCategoryId','categories'));
    }    

    public function store()
    {  
        if(!request('quantity'))
            {
                return redirect()->back()->withErrors("quantity is required");
            }
        if(request('name_type') == 1)
        {
            if(!request('user_id'))
              return redirect()->back()->withErrors("user is required");
            $transactionObj = GoodyTransaction::create(['user_id' => Auth::id(), 'goody_item_id' =>request('item_id'), 'quantity' => -1 * request('quantity'),'reference_type' => 'App\Models\User','reference_id' => request('user_id')]);
            if($transactionObj->isInvalid())
            {
                return redirect()->back()->withErrors($transactionObj->getErrors());
            }
            return redirect('user-report/'.request('user_id'))->withMessage('Added Successfully');
        }
        else
        {
            $obj = GoodyPerson::create(['name' => request('name'), 'description' => request('description'), 'created_by' => Auth::id() , 'approver_id' => request('approved_by')]);   
            if($obj->isInvalid())
            {
                return redirect()->back()->withErrors($obj->getErrors());
            }
            $transactionObj = GoodyTransaction::create(['user_id' => Auth::id(), 'goody_item_id' =>request('item_id'), 'quantity' => -1 * request('quantity'),'reference_type' => 'App\Models\GoodyPerson','reference_id' => $obj->id]);
            if($transactionObj->isInvalid())
            {
                return redirect()->back()->withErrors($transactionObj->getErrors());
            }
            return redirect('goody-assign/'.$obj->id)->withMessage('Added Successfully');
        }
        return redirect()->back()->withErrors("Something went wrong");
    }

    public function destroy($goodyId)
    {
        $obj = GoodyPerson::find($goodyId);
        if($obj->delete())
            return redirect('goody-assign')->withMessage('Deleted Successfully');
        return redirect('goody-assign')->withMessage('Something went wrong'); 
    }

    public function viewReport($goodyPersonId){
        $user = GoodyPerson::find($goodyPersonId);
        $gifts = GoodyItem::get();
        $userTransaction = GoodyTransaction::orderBy('id','DESC')->where('reference_type','App\Models\GoodyPerson')->where('reference_id',$goodyPersonId)->get();
        return view('pages.goody-assign.goody-person-transaction',compact('userTransaction','user','gifts'));
    }
}
