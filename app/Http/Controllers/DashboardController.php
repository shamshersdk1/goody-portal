<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GoodyItem;
use App\Models\GoodyPurchase;
use App\Models\GoodyTransaction;

class DashboardController extends Controller
{
    
    public function index()
    {
        $items = GoodyItem::get();
        $data =[];
        foreach($items as $item) {
            $data[$item->id] = GoodyTransaction::where('goody_item_id',$item->id)->sum('quantity');
        }
        return view('pages.welcome', compact('items','data'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $giftObj = GoodyItem::create(['name' => $data['name'], 'description' => $data['description']]);

        if ($giftObj->isInvalid()) {
            return redirect('goody-items')->withErrors($giftObj->getErrors())->withInput();
        }
        return redirect('goody-items')->with('message', '# '.$giftObj->id.' Item with name "'.$giftObj->name.'" has been created successfully!');
    }

   
    public function show($id)
    {
        $gift = GoodyItem::find($id);
        if (!$gift) {
            return redirect('/goody-items')->withErrors('Appraisal type not found!')->withInput();
        }
        return view('pages.goody-item.edit', compact('gift'));
    }

    
    public function edit($id)
    {
        
    }
    
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $giftObj = GoodyItem::updateOrCreate(['id' => $id], ['name' => $data['name'], 'description' => $data['description']]);
        if ($giftObj->isInvalid()) {
            return redirect('/goody-items/'.$giftObj->id)->withErrors($giftObj->getErrors())->withInput();
        }
        return redirect('/goody-items')->with('message', '# '.$giftObj->id.' Item with name "'.$giftObj->name.'" has been updated successfully!');
    }

    
    public function destroy($id)
    {
        $giftObj = GoodyItem::find($id);
        if ($giftObj && $giftObj->delete()) {
            return redirect('/goody-items')->with('message', '# '.$giftObj->id.' Item with name "'.$giftObj->name.'" has been deleted successfully!');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
