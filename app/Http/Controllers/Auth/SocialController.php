<?php
namespace App\Http\Controllers\Auth;

use App\Helpers\Sys;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\GoodyAccess;

use Config;
use Redirect;
use Request;
use Socialite;

class SocialController extends Controller
{
    public function getSocialRedirect($provider)
    {

        $providerKey = Config::get('services.' . $provider);

        if (empty($providerKey)) {
            return Redirect::back()->with('message', 'No such provider');
        }

        return Socialite::driver($provider)->with(['hd' => 'geekyants.com'])->redirect();

    }

    public function getSocialHandle($provider)
    {
        $isMobile = Sys::checkIsMobile();
        if (!$isMobile) {
            if (Request::input('error') != '') {
                return redirect()->to('/')->withErrors(['Authentication denied by user.']);
            }

           $user = Socialite::driver($provider)->user();
    
            // list($username, $domain) = explode('@', $user->email);
            // if (trim(strtolower($domain)) != 'geekyants.com') {
            //     return redirect()->to('/')->withErrors(['Authentication denied by user - Invalid Domain']);
            // }

            $message = 'Your account is not active, please ask management to activate it.';
            $socialUser = null;
          
            $userExists = User::find(30);
            
            if(!$userExists)
                return redirect()->to('/')->withErrors(['User with email "'.$user->email.'" does not exist']);

            $email = $user->email;

            if (!$user->email) {
                $email = 'missing' . str_random(10);
            }

            if (!empty($userExists)) {
                $socialUser = $userExists;
            }
            
            if ($socialUser->is_active != 1) {

                return redirect()->to('/')->with('message', $message);
            }
            
            $isAccess = GoodyAccess::where('user_id',$userExists->id)->first();
            
            if (!$isAccess) {
                return redirect()->to('/')->withErrors(['Access denied! You do not have sufficient privileges.']);
            }

            auth()->login($socialUser, true);
            return redirect()->to('/dashboard');
        }
        if ($isMobile) {
            if (Request::input('error') != '') {
                return redirect()->to('/')->withErrors(['Authentication denied by user.']);
            }
            $user = Socialite::driver($provider)->user();

            list($username, $domain) = explode('@', $user->email);
            if (trim(strtolower($domain)) != 'geekyants.com') {
                return redirect()->to('/')->withErrors(['Authentication denied by user - Invalid Domain']);
            }

            $message = 'Your account is not active, please ask management to activate it.';
            $socialUser = null;

            //Check is this email present
            $userExists = User::where('email', '=', $user->email)->first();

            $email = $user->email;

            if (!$user->email) {
                $email = 'missing' . str_random(10);
            }

            if (!empty($userExists)) {

                $socialUser = $userExists;

            }

            if ($socialUser->is_active != 1) {

                return redirect()->to('/')->with('message', $message);
            }

            $isAccess = GiftAccess::where('user_id',$socialUser->id)->first();
            
            if (!$isAccess) {
                return redirect()->to('/')->withErrors(['Authentication denied by admin.']);
            }

            auth()->login($socialUser, true);
            return redirect()->to('/dashboard');
        }
    }
}
