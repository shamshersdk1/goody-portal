<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ GoodyTransaction, GoodyItem };
use App\Models\User;
use Auth;


class GoodyTransactionController extends Controller
{
    public function index()
    {
        $items = GoodyItem::all();
        $users = User::all();
        if(!empty(request('userId')) && !empty(request('datefilter')))
        {
            session()->put('forms.user', request('userId'));
            session()->put('forms.datefilter', request('datefilter'));
            $date = explode(" - ",request('datefilter'));
            $from = date_format(date_create($date[0]),"Y/m/d");
            $to = date_format(date_create($date[1]),"Y/m/d");
            $transactions = GoodyTransaction::where('reference_type','App\Models\User')->where('reference_id',request('userId'))->whereDate('updated_at','>=',$from)->whereDate('updated_at','<=',$to)->paginate(50);
        }
        elseif(!empty(request('userId'))){
            session()->put('forms.user', request('userId'));
            $transactions = GoodyTransaction::where('reference_type','App\Models\User')->where('reference_id',request('userId'))->paginate(50);
        }
        elseif(!empty(request('datefilter')))
        {
            session()->put('forms.datefilter', request('datefilter'));
            $date = explode(" - ",request('datefilter'));
            $from = date_format(date_create($date[0]),"Y/m/d");
            $to = date_format(date_create($date[1]),"Y/m/d");
            $transactions = GoodyTransaction::whereDate('updated_at','>=',$from)->whereDate('updated_at','<=',$to)->paginate(50);
          
        } else
        {
            session()->pull('forms.user', request('userId'));
            session()->put('forms.datefilter', request('datefilter'));
            $transactions = GoodyTransaction::orderBy('id', 'DESC')->paginate(50);
        }
        return view('pages.goody-transaction.index', compact('transactions','users','items'));
    }

    public function userReport()
    {
        $users = GoodyTransaction::where('reference_type','App\Models\User')->groupBy('reference_id')->get();
        $giftItems = GoodyItem::get();
        $data =[];
        foreach($users as $user) {
            $items =[];
            foreach($giftItems as $item) {
                $temp =[];
                $quantity = GoodyTransaction::where('reference_type','App\Models\User')->where('goody_item_id',$item->id)->sum('quantity');
                $items[$item->id]  = $quantity;
            }    
            $data[$user->reference->id] = $items;
        }
         $users = User::orderBy('is_active', 'DESC')->orderBy('employee_id', 'ASC')->get();
        return view('pages.goody-transaction.report', compact('users','data','giftItems'));
    }

    public function viewReport($user_id){
        $users = User::get();
        $user = User::find($user_id);
        $gifts = GoodyItem::get();
        $userTransaction = GoodyTransaction::orderBy('id','DESC')->where('reference_type','App\Models\User')->where('reference_id',$user_id)->get();
        return view('pages.goody-transaction.show',compact('users','userTransaction','user','gifts'));
        
    }

    public function updateUserGoody($user_id){
        
        $data =  request()->all();
        $user= User::find($user_id);
        $item = GoodyItem::find($data['goody_item_id']);
        $goodyAssignToUser = GoodyTransaction::where('goody_item_id',$item->id)->where('reference_type','App\Models\User')->where('reference_id',$user_id)->sum('quantity');
        $available_qty = GoodyTransaction::where('goody_item_id',$item->id)->sum('quantity');

        if (!$item) {
            return redirect()->back()->withErrors("Required all fields")->withInput();
        }
        if ($data['quantity'] == 0) {
            return redirect()->back()->withErrors("Please maintain at least 1 quantity")->withInput();
        }

        if($goodyAssignToUser > $data['quantity']){
            return redirect()->back()->withErrors("Number of '".$item->name."' quantity assigned to user is less than ".abs($data['quantity']).". Cannot remove the item quantity.")->withInput();
        }
        
        if(!$available_qty && $data['quantity'] > 0){
            return redirect()->back()->withErrors("Sorry! $item->name are out of stock")->withInput();
        }

        if(!($available_qty >= $data['quantity'])){
            return redirect()->back()->withErrors("Sorry! You have left only $available_qty $item->name")->withInput();
        }
        
        $transactionObj = GoodyTransaction::create(['user_id' => Auth::id(),'goody_item_id' => $data['goody_item_id'], 'quantity' => -1 * $data['quantity'],'reference_type' => 'App\Models\User','reference_id' => $user_id]);
        if ($transactionObj->isInvalid()) {
            return redirect('/user-report/'.$user_id.'/add-goody')->withErrors($transactionObj->getErrors())->withInput();
        }

        if($data['quantity'] < 0){
            return redirect('/user-report/'.$user_id)->with('message', $user->name.' successfully removed '.abs($data['quantity']).' "'.$transactionObj->item->name.'"');
        }
        return redirect('/user-report/'.$user_id)->with('message', '"'.$transactionObj->item->name.'" successfully assigned to '. $user->name);


    }

    public function assignGoody() {
        $items = GoodyItem::all();
        $users = User::where('is_active' , 1)->get();
        return view('pages.goody-assign.index', compact('items' ,'users'));
    }    
}
