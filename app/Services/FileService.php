<?php namespace App\Services;

use App\Models\File;
use Redirect;
use Validator;

class FileService
{

    // /**
    //  * Get a validator for an incoming registration request.
    //  *
    //  * @param  array  $data
    //  * @return \Illuminate\Contracts\Validation\Validator
    //  */
    public static function getFolder()
    {
        $path = date('Y/m/d');
        return $path;
    }

    public static function uploadFile($file, $reference_id,$reference_type)
    {
        if($file){
            $getFolder = self::getFolder();
            $originalName = $file->getClientOriginalName();
            $fileType = $file->extension();
            $filePath = $file->store('public/'.$getFolder);
            if($filePath)
            {
                $fileObj = File::create(['name' => $originalName, 'type' => $fileType, 'is_external' => 0 , 'path' => "storage/".str_replace("public/","",$filePath) , 'reference_type' => $reference_type , 'reference_id' => $reference_id]);
                if($fileObj->isInvalid())
                {
                    return ['status' => true, 'message' => "File Upload Error"];
                }
            }
        }
        return ['status' => false, 'message' => "File Added"];
    }

    public static function updateUploadFile($file, $reference_id,$reference_type)
    {  
        if($reference_id){
            $temp= File::where('reference_id',$reference_id)->first();
            if ($temp) {
                $temp->delete();
            }
        }     

        if($file){
        $getFolder = self::getFolder();
        $originalName = $file->getClientOriginalName();
        $fileType = $file->extension();
        $filePath = $file->store('public/'.$getFolder);
        if($filePath)
        {   
            $fileObj = File::create(['name' => $originalName, 'type' => $fileType, 'is_external' => 0 , 'path' => "storage/".str_replace("public/","",$filePath) , 'reference_type' => $reference_type , 'reference_id' => $reference_id]);
            if($fileObj->isInvalid())
            {
                return ['status' => true, 'message' => "File Upload Error"];
            }
        }
        }
        return ['status' => false, 'message' => "File Updated"];
    }
}
