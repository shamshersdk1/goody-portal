<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class GoodyPerson extends Model
{
    use ValidatingTrait;
    protected $table = "goody_persons";
    protected $fillable = ['name', 'description','created_by','approver_id'];
    protected $rules = [
        'name'   => 'required',
        'created_by' => 'required',
        'approver_id' => 'required'
    ];

    public function createdBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    public function approvedBy()
    {
        return $this->belongsTo('App\Models\User', 'approver_id', 'id');
    }
}
