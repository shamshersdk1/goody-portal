<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class GoodyCategory extends Model
{
    use ValidatingTrait;

    public $timestamps = false;

    protected $table = 'goody_categories';
    protected $fillable = ['name'];
   

    protected $rules = [
        'name'   => 'required',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\GoodyItem', 'category_id','id');
    }
}
