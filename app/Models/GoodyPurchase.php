<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;


class GoodyPurchase extends Model implements Auditable
{
    use SoftDeletes;
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;
    protected $fillable = ['user_id', 'goody_item_id', 'quantity', 'total_amount','note','invoice_number','purchase_order','is_approved'];
    protected $rules = [
        'goody_item_id'   => 'required | exists:goody_items,id',
        'quantity' => 'required | numeric',
    ];

    public function goodyItem()
    {
        return $this->belongsTo('App\Models\GoodyItem', 'goody_item_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function attachments()
    {
        return $this->morphMany('App\Models\File', 'reference');
    }

    public function transactions()
    {
        return $this->morphMany('App\Models\GoodyTransaction', 'reference');
    }
    
}
