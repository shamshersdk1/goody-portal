<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;


class File extends Model
{
    use ValidatingTrait;
    use SoftDeletes;
    
    protected $fillable = ['name','type','is_external','path','reference_type','reference_id'];

    protected $rules = [
        'name'   => 'required',
        'type' => 'required',
        'path' => 'required',
        'reference_type' => 'required',
        'reference_id' => 'required',
    ];

    public function reference()
    {
        return $this->morphTo();
    }
}
