<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;


class GoodyItem extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    
    protected $fillable = ['name', 'description','category_id'];
    protected $rules = [
        'name'   => 'required',
        'description' => 'required',
        'category_id' => 'required'
    ];
    
    public function image()
    {
        return $this->morphOne('App\Models\File', 'reference');
    }
    public function goodyPurchase()
    {
        return $this->hasMany('App\Models\GoodyPurchase', 'goody_item_id','id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\GoodyCategory', 'category_id', 'id');
    }    
}
