<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class GoodyTransaction extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;
    protected $fillable = ['user_id', 'goody_item_id', 'quantity', 'reference_type', 'reference_id'];
    protected $rules = [
        'goody_item_id'   => 'required',
        'quantity' => 'required | numeric ',
    ];
    
    public function reference()
    {
        return $this->morphTo();
    }
     public function item()
    {
        return $this->belongsTo('App\Models\GoodyItem', 'goody_item_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id','id');
    }
}
