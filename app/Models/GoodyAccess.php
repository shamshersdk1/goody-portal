<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class GoodyAccess extends Model implements Auditable
{

    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'goody_access';

    protected $fillable = ['user_id'];

}
