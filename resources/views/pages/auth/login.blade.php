@extends('pages.session.app')
@section('main-content')
    <div class="container">
        <div class="row justify-content-center xs-pt-50">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header text-center"><h4 class="xs-mb-0">{{ __('Login') }}</h4></div>
                        <div class="card-body text-center py-4">
                                <a href="{{ route('social.redirect', ['provider' => 'google']) }}" class="btn btn-lg btn-danger"><i class="fa fa-google-plus" aria-hidden="true"></i> Sign in with Google</a>
                        </div>
                    </div>
            </div>
        </div>
        </div>
    </div>
@endsection
