<div class=" p-2 d-flex justify-center border mb-3" style="justify-content: space-between;  position: -webkit-sticky; position: sticky; top:50px; z-index: 999; background: #fff;">
    <div>
        @if($giftPurchase->goodyItem && $giftPurchase->goodyItem->image)
            <div class="font-14 font-500 text-center text-primary">
                <img src="{{'/'.$giftPurchase->goodyItem->image->path ?? '#'}}" height="42" width="42"/> 
            </div>
        @endif
    </div>
    <div>
        <div class="text-muted">Goody Item</div>
        <div class="font-14 font-500 text-center text-primary">{{$giftPurchase->goodyItem->name}}</div>
    </div>
    <div>
        <div class="text-muted">Quantity</div>
        <div class="font-14 font-500 text-center">{{number_format($giftPurchase->quantity)}}</div>
    </div>
    <div>
        <div class="text-muted">Total Amount</div>
        <div class="font-14 font-500 text-center">{{number_format($giftPurchase->total_amount)}}</div>
    </div>
    <div>
        <div class="text-muted">Stock
        </div>
        <div class="font-16 font-500 text-center">
            @if($giftPurchase->is_approved )
                 <span class="badge badge-success" style="font-size:12px">Approved</span>
            @else
                <span class="badge badge-danger" style="font-size:12px">Pending</span>
            @endif
      
       </div>
    </div>
   
</div>
