@extends('layouts.dashboard')
@section('title')
Goody Purchase  | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-8">
				<h1 class="page-title">Goody Purchase</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="{{ url('/goody-purchase') }}">Goody Purchase</a></li>
					<li class="breadcrumb-item active">{{$giftPurchase->goodyItem->name}}</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
             </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger m-1 p-2">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span>
                        </div>
		              @endforeach
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
</div>

@if($readonly)

	@include('pages.goody-purchase.purchase-detail')

	<div class="card p-3">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-sm-12">
					<label>Invoice Number: </label><span class="ml-2">{{$giftPurchase->invoice_number}}</span><br>
					<label>Purchase Order: </label><span class="ml-2">{{$giftPurchase->invoice_number}}</span><br>
					<label >Created By: </label><span class="ml-2">{{$giftPurchase->user->name}}</span><br>
					<label>Updated At: </label><span class="ml-2">{{datetime_in_view($giftPurchase->updated_at)}}</span><br>
					<label>Created At: </label><span class="ml-2">{{datetime_in_view($giftPurchase->created_at)}}</span><br>
					<label>Note: </label><span class="ml-2">{{$giftPurchase->note}}</span><br>
					<label >Attachments: </label>
						<span class="ml-2">
						@if($giftPurchase->attachments)
								@foreach($giftPurchase->attachments as $attachment)
								<div class="col-md-12 p-1" >
									<span class="p-1">{{$attachment->name}}</span>
									<a type="button" class="btn btn-danger btn-sm" target="_blank" href="/{{$attachment['path'] ?? '#'}}">View</a>
								</div>
								@endforeach
						@endif
						</span>
				</div>
			</div>
		</div>
	</div>
	</div>


@endif
@if(!$readonly)
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="panel-body">						
                    <br>
                    {{ Form::open(['goody-purchase/'.$giftPurchase->id, 'method' => 'put']) }}
				    {{ Form::token() }}
					<div class="form-group row">
						{{ Form::label('item', 'Goody Item :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{ Form::select('goody_item_id', $gifts->pluck('name','id'),$giftPurchase->goody_item_id, array('class' => 'form-control', 'id' => 'userSelect1' ,'placeholder' => 'Select Item'))}}
						</div>
					</div>
					<div class="form-group row">
                    	{{ Form::label('quantity', 'Quantity :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{ Form::text('quantity', $giftPurchase->quantity ,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group row">
                    	{{ Form::label('amount', 'Total Amount :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('total_amount', $giftPurchase->total_amount ,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group row">
						{{ Form::label('invoice', 'Invoice Number :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
							<div class="col-md-4" style="padding:0">
								{{ Form::text('invoice_number', $giftPurchase->invoice_number ,['class' => 'form-control']) }}
							</div>
					</div>
					<div class="form-group row">
						{{ Form::label('purchaseOrder', 'Purchase Order :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
							<div class="col-md-4" style="padding:0">
								{{ Form::text('purchase_order', $giftPurchase->purchase_order ,['class' => 'form-control']) }}
							</div>
					</div>
					<div class="form-group row">
                    {{ Form::label('note', 'Note :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::textarea('note', $giftPurchase->note ,['class' => 'form-control','rows'=>2]) }}
						</div>
					</div>
					
					@if(!$readonly)
					<div class="form-group row">
						<label class="col-md-4 text-right" style="padding-top:1%;align:right;">Stock:</label>
							<div class="col-md-4">
								<span class="badge badge-danger p-1%">Pending</span>
							</div>
					</div>
					@endif

					<div class="form-group row">
						<label class="col-md-4 text-right" style="padding-top:1%;align:right;">Attachments:</label>
						<div class="col-md-4">
							@if($giftPurchase->attachments)
							<table>
								@foreach($giftPurchase->attachments as $attachment)
								<tr>
									<td><span class="p-1">{{$attachment->name}}</span></td>
									<td><a type="button" class="btn btn-danger btn-sm" target="_blank" href="/{{$attachment['path'] ?? '#'}}">View</a></td>
								<tr>
								@endforeach
							</table>
							@endif
						</div>
					</div>
					
					
					<div class="col-md-12">
						@if(!$readonly)
							{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
						@endif
					</div>
					
                    {{ Form::close() }}
				</div>
			</div>
		</div>
	</div>

@endif

@endsection

