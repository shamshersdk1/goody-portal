@extends('layouts.dashboard')
@section('title')
Goody Purchase | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
		<div class="col-sm-12">
			<h1 class="page-title">Goody Purchase</h1>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
				<li class="breadcrumb-item active">Goody Purchase</li>
			</ol>
		</div>
		<div class="col-sm-4 text-right m-t-10">
		</div>
	</div>

    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
					@foreach ($errors->all() as $error)
					<div class="alert alert-danger m-1 p-2">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<span>{{ $error }}</span>
					</div>
		              @endforeach
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
</div>

	<div class="card border">
        <div class="card-body">
			{{ Form::open(['url' => 'goody-purchase', 'method' => 'post','enctype' => 'multipart/form-data']) }}
			{{csrf_field()}}
			<div class="row d-flex justify-content-center">
				<div class="col-sm-5">


				<div class="row mb-3">
					<label class="col-md-4">Select Category: </label>
					@if(isset($categories))
						<select class="col-md-8" id="selectid2" name="category_id">
							<option value=""></option>
							@foreach($categories as $x)
								@if($x->id == $goodyCategoryId)
									<option value="{{$x->id}}"  selected>{{$x->name}}</option>
								@else
									<option value="{{$x->id}}" >{{$x->name}}</option>
								@endif
							@endforeach
						</select>
					@endif
				</div>




					<div class="row mb-3">
						{{ Form::label('item', 'Goody Item :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
							<select class="form-control" id="selectid2" name="goody_item_id">
								<option value=""></option>
								@foreach($gifts as $x)
									<option value="{{$x->id}}" >{{$x->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row mb-3">
						{{ Form::label('quantity', 'Quantity :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
							{{ Form::number('quantity', '',['class' => 'form-control','min'=>1]) }}
						</div>
					</div>
					<div class="row mb-3">
						{{ Form::label('amount', 'Total Amount :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
							{{ Form::text('total_amount', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="row mb-3">
						{{ Form::label('image', 'Attachments :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
							<input type="file" name="goody_purchase_attachments[]"  class="form-control" multiple>
						</div>
					</div>	
				</div>
				<div class="col-sm-5">
					<div class="row mb-3">
						{{ Form::label('invoiceNumber', 'Invoice Number :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
							{{ Form::text('invoice_number', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="row mb-3">
						{{ Form::label('PO', 'Purchase Order :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
							{{ Form::text('purchase_order', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="row mb-3">
						{{ Form::label('note', 'Note :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
							{{ Form::textarea('note', '',['class' => 'form-control','rows'=>3]) }}
						</div>
					</div>		
				</div>
				<div class="col-md-12 text-center">
					<div class="row text-center mt-2">
						<div class="col-md-12">
							{{ Form::submit('Add',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
						</div>
					</div>
				</div>
			</div>
			{{ Form::close() }}
        </div>  
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="table-responsive">
							<table class="table table-striped table-outer-border no-margin table-sm" id="type-table">
								<thead>
									<th width="10%">#</th>
									<th width="15%">Item</th>
									<th width="10%">Quantity</th>
									<th width="15%">Total Amount</th>
									<th width="15%">Stock</th>
									<th width="15%">Created By</th>
									<th width="15%">Created At</th>
									<th width="15%">Updated At</th>
									<th width="25%" class="text-right">Actions</th>
								</thead> 
								@if(count($giftPurchase)>0)
									@foreach($giftPurchase as $index => $gift)
										<tr>
											<td class="td-text">{{$index+1}}</td>
											<td class="td-text"> {{$gift->goodyItem->name}}</td>
											<td class="td-text"> {{$gift->quantity}}</td>
											<td class="td-text"> {{number_format($gift->total_amount)}}</td>
											<td class="td-text">
												@if($gift->is_approved)
													<span class="badge badge-success">Approved</span>
												@else
													<span class="badge badge-danger">Pending</span>
												@endif
												{{$gift->userquantity}}
											</td>
											<td class="td-text"> {{$gift->user->name}}</td>
											<td class="td-text"> {{datetime_in_view($gift->created_at)}}</td>
											<td class="td-text"> {{datetime_in_view($gift->updated_at)}}</td>
											<td class="text-right">
												@if(!$gift->is_approved)
												@endif
												@if($gift->is_approved)
													<a href="/goody-purchase/{{$gift->id}}" class="btn btn-dark crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
													<div style="display:inline-block;" class="deleteform">
														{{ Form::open(['url' => '/goody-purchase/'.$gift->id, 'method' => 'delete']) }}
														{{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
														{{ Form::close() }}
													</div>
												@else
													<a href="/goody-purchase/{{$gift->id}}/approve" onclick = 'confirm("Are you sure approve this stock entry?")' class="btn btn-info crud-btn btn-sm"><i class="fa fa-check btn-icon-space" aria-hidden="true"></i>Approve</a>
													<a href="/goody-purchase/{{$gift->id}}" class="btn btn-warning crud-btn btn-sm">Edit</a>
												@endif
												
											</td>
										</tr>
									@endforeach
								@else
									<tr >
										<td colspan="9" class="text-center">No Goody Purchase added.</td>
									</tr>
								@endif  
											
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
@parent
<script>
$('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/goody-purchase/?goody_category_id="+optionValue;
        }
    });
    $('#selectid2').select2({
			allowClear:true
		});

</script>
<script>
    $(document).ready(function() {
		var t = $('#type-table').DataTable({
			fixedHeader: false,
            scrollY:        "500px",
            scrollX:        false,
			paging:         false,
			dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
		});
	})
    $('#selectid2').select2({
        placeholder: 'Select Goody',
        allowClear:true
    });
</script>
@endsection
