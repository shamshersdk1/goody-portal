@extends('layouts.dashboard')
@section('title')
Goody Items  | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
		<div class="col-sm-8">
			<h1 class="page-title">Goody Items</h1>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
				<li class="breadcrumb-item"><a href="{{ url('/goody-items') }}">Goody Items</a></li>
				<li class="breadcrumb-item active">{{$gift->name}}</li>
			</ol>
		</div>
		<div class="col-sm-4 text-right m-t-10">
			<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		@if(!empty($errors->all()))
				@foreach ($errors->all() as $error)
					<div class="alert alert-danger m-1 p-2">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<span>{{ $error }}</span>
					</div>
					@endforeach
		@endif
		@if (session('message'))
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<span>{{ session('message') }}</span><br/>
			</div>
		@endif
	</div>
</div>
<div class="card">
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body">
				<br>
			{{ Form::open(['url' => 'goody-items/'.$gift->id, 'method' => 'put','enctype' => 'multipart/form-data']) }}
			{{ Form::token() }}


			<div class="form-group row">
                    {{ Form::Label('cate', 'Goody Category:', array('class' =>'col-md-4' ,'align' => 'right')) }}
                    <div class="col-md-4" style="padding:0">
                        {{ Form::select('category_id', $categories->pluck('name','id'),$gift->category_id, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Category'))}}
                    </div>
				</div>
				
			<div class="form-group row">
				{{ Form::label('name', 'Name :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
				<div class="col-md-4" style="padding:0">
					{{ Form::text('name', $gift->name,['class' => 'form-control']) }}
				</div>
			</div>
			<div class="form-group row">
				{{ Form::label('description', 'Description :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
				<div class="col-md-4" style="padding:0">
					{{ Form::textarea('description', $gift->description,['class' => 'form-control','rows'=>3]) }}
				</div>
			</div>
			<div class="form-group row">
				{{ Form::label('image', 'Image :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
				<div class="col-md-4" style="padding:0">
				<input type="file" name="gift_image" class="form-control">
				</div>
			</div>
			<div class="col-md-12">
				@if($gift->image)
				<a href="{{'/'.$gift->image->path }}" target="_blank" ><img class="p-3" href="/" style="display: block;margin: 0 auto;" src="{{'/'.$gift->image->path }}" height="70" width="75" alt="Goody Image"/> </a>
				@endif
			</div>
			<div class="col-md-12">
				{{ Form::submit('Update',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
			</div>
			{{ Form::close() }}
			
			</div>
		</div>
	</div>
</div>
@endsection

