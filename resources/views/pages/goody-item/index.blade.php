@extends('layouts.dashboard')
@section('title')
Goody Items | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h1 class="page-title">Goody Items</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Goody Items</li>
            </ol>
        </div>
        <div class="col-sm-6 text-right">
            <a href="/goody-items/create" class="btn btn-success crud-btn btn-sm">Add Item</a>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger m-1 p-2">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span>
                </div>  
                @endforeach
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
</div>
@foreach($categories as $category)
    <h5>{{$category->name}}</h5>
    <div class="row">
    @foreach($category->items as $item)
        <div class="col-md-3">
            <div class="card border-secondary">
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4"><img class="card-img-top" src="{{$item->image->path ?? '#'}}" alt="Card image cap"></div>
                        <div class="col-sm-8">
                            <h6 class="card-title">{{$item->name}}</h6>
                            <p class="card-text">Stock: {{$data[$item->id]}}</p>
                        </div>
                    </div>
                    <div class="mt-3">
                        <a href="/goody-assign/?category_id={{$category->id}}&goody_item_id={{$item->id}}" class="btn btn-sm btn-primary">Assign</a>
                        <a href="/goody-items/{{$item->id}}" class="btn btn-sm btn-info crud-btn"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
                        <div style="display:inline-block; margin:0;" class="deleteform">
                            {{ Form::open(['url' => '/goody-items/'.$item->id, 'method' => 'delete']) }}
                            {{ Form::submit('Delete',['class' => 'btn btn-sm btn-danger','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
    <hr />
@endforeach


@if($otherGifts && (count($otherGifts) > 0) )
    <h5>Others</h5>
    <div class="row">
    @foreach($otherGifts as $item)
        <div class="col-md-3">
            <div class="card border-secondary">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4"><img class="card-img-top" src="{{$item->image->path ?? '#'}}" alt="Card image cap"></div>
                        <div class="col-sm-8">
                            <h6 class="card-title">{{$item->name}}</h6>
                            <p class="card-text">Stock: {{$data[$item->id]}}</p>
                        </div>
                    </div>
                    <div class="mt-3">
                        <a href="/goody-assign/?category_id={{$category->id}}&goody_item_id={{$item->id}}" class="btn btn-sm btn-primary">Assign</a>
                        <a href="/goody-items/{{$item->id}}" class="btn btn-sm btn-info crud-btn"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
                        <div style="display:inline-block; margin:0;" class="deleteform">
                            {{ Form::open(['url' => '/goody-items/'.$item->id, 'method' => 'delete']) }}
                            {{ Form::submit('Delete',['class' => 'btn btn-sm btn-danger','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
    <hr />
@endif
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
		var t = $('#goody-table').DataTable({
			fixedHeader: false,
            scrollY:        "500px",
            paging:         false,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            
		});
	})
</script>
<script>
    $(document).ready(function() {
        $(".userSelect1").select2();
    });
</script>
@endsection