@extends('layouts.dashboard')
@section('title')
Goody Category | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-12">
				<h1 class="page-title">Goody Category</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item active">Goody Category</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
            </div>

	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
                    @foreach ($errors->all() as $error)
		            <div class="alert alert-danger m-1 p-2">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span>
		            </div>
                        
		              @endforeach
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
</div>
 
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">
                <br>
                {{ Form::open(['url' => 'goody-category', 'method' => 'post' ,'enctype' => 'multipart/form-data']) }}
                {{csrf_field()}}
                <div class="form-group row">
                    {{ Form::label('name', 'Name :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
                    <div class="col-md-4" style="padding:0">
                        {{ Form::text('name', '',['class' => 'form-control']) }}
                    </div>
                </div>
               
                <div class="col-md-12">
                    {{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-default">
                    <div class="table-responsive">
                        <table class="table table-striped table-outer-border no-margin table-sm" id="goody-table">
                            <thead>
                                <th class="td-text">#</th>
                                <th class="td-text">Name</th>
             
                                <th class="text-right">Actions</th>
                            </thead> 
                            @if(isset($goodyCategories))
                                @foreach($goodyCategories as $index => $goodyCategory)
                                    <tr>
                                        <td class="td-text">{{$index+1}}</td>
                                        <td class="td-text"> {{$goodyCategory->name}}</td>
                                   
                                        <td class="text-right">
                                            <div style="display:inline-block;" class="deleteform">
                                            {{ Form::open(['url' => '/goody-category/'.$goodyCategory->id, 'method' => 'delete']) }}
                                            {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                            {{ Form::close() }}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr >
                                    <td colspan="6" class="text-center"><big>No Goody Categories added.</big></td>
                                </tr>
                            @endif                  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@parent
<script>
    $(document).ready(function() {
		var t = $('#goody-table').DataTable({
			fixedHeader: false,
            scrollY:        "500px",
            paging:         false,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            
		});
	})
</script>
@endsection