@extends('layouts.dashboard')
@section('title')
Goody Transaction | @parent
@endsection
@section('main-content')
<div class="page-title-box" >
    <div class="row align-items-center">
            <div class="col-sm-12">
                <h1 class="page-title">User Goody Report</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item active">User Goody Report</li>
                </ol>
            </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <table class="table table-striped table-outer-border no-margin table-sm" id="report-table">
                <thead>
                    <th class="td-text">#</th>
                    <th class="td-text">Employee Name</th>
                    <th class="td-text">EMP ID</th>
                    <th class="td-text">Joining Date</th>
                    <th class="td-text">Status</th>
                    {{--  @foreach($giftItems as $giftItem)
                        <th class="td-text">{{ $giftItem->name }}</th>
                    @endforeach  --}}
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($users as $index=> $user)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->employee_id}}</td>
                        <td>{{date_in_view($user->joining_date)}}</td>
                        <td>
                        @if($user->is_active)
                            <span class="badge badge-success">Active</span>
                        @else
                            <span class="badge badge-danger">Deactived</span>
                        @endif
                        </td>
                        {{--  @foreach($giftItems as $giftItem)
                            <td>
                                {{ $data[$user->id][$giftItem->id]??0 }}
                            </td>
                        @endforeach  --}}
                        <td>
                            <a href="user-report/{{$user->id}}" class="btn btn-dark btn-sm">View Detail</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
		var t = $('#report-table').DataTable({
			fixedHeader: false,
            scrollX:        false,
            paging:         false,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
		});
	})
</script>
@endsection