@extends('layouts.dashboard')
@section('title')
Goody Transaction | @parent
@endsection
@section('main-content')

<div class="page-title-box" >
    <div class="row align-items-center">
		<div class="col-sm-12">
			<h1 class="page-title">Goody Transaction</h1>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
				<li class="breadcrumb-item active">Goody Transaction</li>
			</ol>
		</div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
</div>	

<div class="card">
    <div class="row">
		<div class="col-md-6">
			<form action="/goody-transaction" method="GET">
				<div class="col-md-12 mt-1 p-1% d-flex" style="justify-content: space-between;">
					<select class="col-sm-4 user-list" id="selectid3" name="userId" placeholder= "Select an user">
						<option value=""></option>
						@foreach($users as $x)
							<option value="{{$x->id}}" @if( session('forms.user')  == $x->id) selected="selected" @endif>{{$x->name}}</option>
						@endforeach
					</select>
					<div>
						<input type="text" name="datefilter" value='{{session('forms.datefilter')??""}}' />
						<button type="submit" class="btn btn-info btn-sm crude-btn"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
					</div>
					<a type="button" href="/goody-transaction" class="btn btn-danger btn-sm crude-btn">Clear</a>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="card">
	<table class="table table-striped table-outer-border no-margin table-sm" id="transaction-table">
		<thead>
			<th class="td-text">#</th>
			<th class="td-text">Transaction Type</th>
			<th class="td-text">Quantity</th>
			<th class="td-text">Created By</th>
			<th class="td-text">Created At</th>
			{{--  <th class="td-text">Updated At</th>  --}}
		</thead>
		<tbody>
			@if(count($transactions)>0)
				@foreach($transactions as $transaction)
				<tr>
					<td class="td-text">{{$transaction->id}}</td>
					<td class="td-text">
						@if($transaction->reference_type == 'App\Models\GoodyPurchase')
							<a href="goody-purchase/{{$transaction->reference_id}}" target="_blank">Purcahse Stock Added | {{  $transaction->item->name??''}}</a>
						@elseif($transaction->reference_type == 'App\Models\User')
							<a href="user-report/{{$transaction->reference_id}}" target="_blank">Item Assigned | {{$transaction->item->name??''}} | {{$transaction->reference->name}} | {{$transaction->reference->employee_id}}</a>
						@elseif($transaction->reference_type == 'App\Models\GoodyPerson')
                            <a href="goody-assign/{{$transaction->reference_id}}" target="_blank">Item Assigned | {{$transaction->item->name}} | {{$transaction->reference->name}}</a>	
						@endif
						</td>
					<td class="td-text">{{$transaction->quantity}}</td>
					<td class="td-text">{{$transaction->user->name??''}}</td>
					<td class="td-text">{{datetime_in_view($transaction->created_at) }}</td>
					{{--  <td class="td-text">{{datetime_in_view($transaction->updated_at)}}</td>  --}}
				</tr>
				@endforeach
			@else
				<td colspan="5" style="text-align:center;"><span class="align-center"><big>No Appraisal Bonus Type</big></span></td>
			@endif
		</tbody>
	</table>
	{{ $transactions->links() }}
</div>


@endsection
@section('js')
@parent
	<script>
		$(function() {
			$('input[name="datefilter"]').daterangepicker({
				autoUpdateInput: false,
				locale: {
					cancelLabel: 'Clear'
				}
			});

			$('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
			});

			$('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
			});
			
		});
		$('#selectid3').select2({
				placeholder:"Select User",
				allowClear:true
			});
	</script>
@endsection