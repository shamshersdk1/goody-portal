@extends('layouts.dashboard')
@section('title')
Goody Transaction | @parent
@endsection
@section('main-content')

<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-12">
				<h1 class="page-title">User Goody Report</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="/user-report">User Goody Report</a></li>
					<li class="breadcrumb-item active">{{$user->id}}</li>
                </ol>
			</div>
			
    </div>
    
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
                    @foreach ($errors->all() as $error)
		            <div class="alert alert-danger m-1 p-2">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span>
		            </div>
                        
		              @endforeach
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
</div>
@if(isset($users))
<div class="row mb-2">
    <div class="col-sm-12 d-flex justify-content-end align-items-center">
        <label class="m-0 mr-1">Select User: </label>
        <select id="selectid2" name="user">
            <option value=""></option>
            @foreach($users as $x)
                <option value="{{$x->id}}" >{{$x->name.' | '.$x->employee_id}}</option>
            @endforeach
        </select>
    </div> 
</div>   
@endif

@include('layouts/user-detail')

<div class="card card-default border mt-2">
    <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <h5 class="mt-4 text-center text-primary mr-3">Assign Goody</h5>
            <div class="panel-body">
                <br>
                {{ Form::open(['url' => "user-report/{$user->id}", 'method' => 'post']) }}
                <div class="form-group row d-flex justify-content-center">
                    <div class="col-md-5 mt-1 p-1%">
                        <select class="form-control" id="goody_item_id" name="goody_item_id">
                            <option value=""></option>
                            @foreach($gifts as $x)
                                <option value="{{$x->id}}" >{{$x->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-5">
                        {{ Form::number('quantity', '',['class' => 'form-control','placeholder' => 'Enter Quantity']) }}
                    </div>
                <div class="col-md-2">
                        {{ Form::submit('Add',['class' => 'btn btn-success','style' => 'margin: 0 auto']) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-default">
            <div class="table-responsive">
            <table class="table table-striped table-outer-border no-margin table-sm" id="transaction-table">
                <thead>
                    <tr>
                        <th colspan="4"><strong>Goody Transactions</strong></th>
                    </tr>
                    <th width="10%">#</th>
                    <th width="10%">Goody Name</th>
                    <th width="15%">Quantity</th>
                    <th width="15%">Created By</th>
                    <th width="15%">Created At</th>
                </thead> 
                @if(count($userTransaction)>0)
                    @foreach($userTransaction as $index => $transaction)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text"> {{$transaction->item->name??''}}</td>
                            <td class="td-text"> {{abs($transaction->quantity)}}&nbsp;
                                @if($transaction->quantity < 0)
                                <span class="badge badge-success">Assigned</span>
                                @else
                                <span class="badge badge-danger">Removed</span>
                                @endif
                            </td>
                            <td class="td-text"> {{$transaction->user->name}}</td>
                            <td class="td-text"> {{datetime_in_view($transaction->created_at)}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr >
                        <td colspan="5" class="text-center"><big>No Goody Transactions added.</big></td>
                    </tr>
                @endif                  
            </table>
            </div>
        </div>
    </div>
</div>


@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
		var t = $('#transaction-table').DataTable({
			fixedHeader: true,
            scrollX:        false,
            paging:         false,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
        });
    });
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/user-report/"+optionValue;
        }
    });
    $('#selectid2').select2({
        placeholder: '{{$user ? $user->name: 'Select User'}}',
        allowClear:true
    });
    $('#goody_item_id').select2({
        placeholder: 'Select Goody',
        allowClear:true
    });
        
</script>
@endsection