@extends('layouts.dashboard')
@section('title')
Dashboard
@endsection
@section('main-content')

    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>
    @forelse($items as $item)
        <div class="card border">
            <div class="card-body">
                Item Name : {{ $item->name }}<br/>
                Current Stock : {{ $data[$item->id] }}
            </div>   
        </div>
    @empty
        <div class="card border">
            <div class="card-body">
                No item stock found
            </div>
        </div>
    @endforelse
</div>
@endsection