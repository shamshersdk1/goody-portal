@extends('layouts.dashboard')
@section('title')
Goody Assign
@endsection
@section('main-content')
   <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">Goody Assign</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
        <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
    </div>





<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">
                <br>
                {{ Form::open(['url' => 'goody-assign', 'method' => 'post' ,'enctype' => 'multipart/form-data']) }}
                {{csrf_field()}}


                <div class="form-group row" >
                    {{ Form::Label('goodyIterm', 'Goody Category:',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right']) }}
                    <div class="col-md-4" style="padding:0">
                        {{ Form::select('category_id', $categories->pluck('name','id'),$goodyCategoryId ?? null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Goody Item'))}}
                    </div>
                </div>

                <div class="form-group row" >
                    {{ Form::Label('goodyIterm', 'Goody Item:',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right']) }}
                    <div class="col-md-4" style="padding:0">
                        {{ Form::select('item_id', $items->pluck('name','id'),$goodyItemId ?? null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Goody Item'))}}
                    </div>
                </div>

                <div class="form-group row">
                    {{ Form::label('name', 'Assign To :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
                    <div class="col-md-8" style="padding:0">
                        {{ Form::radio('name_type',true,true,['class' => 'form-control', 'onclick' => 'myFunction()' , 'id' => 'myCheck' ]) }} Employee
                        {{ Form::radio('name_type',false,false,['class' => 'form-control' , 'onclick' => 'myFunction()', 'id' => 'myCheck2' ]) }} Other
                    </div>
                </div>
                <div class="form-group row" id="user-box">
                    {{ Form::Label('user', 'User Name:',['class' => 'col-md-4' ,'align' => 'right']) }}
                    <div class="col-md-4" style="padding:0">
                        {{ Form::select('user_id', $users->pluck('name','id'),null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Employee'))}}
                    </div>
                </div>
                <div class="form-group row" id="name-box" style="visibility:hidden">
                    {{ Form::label('name', 'Name :',['class' => 'col-md-4','align' => 'right'])}}
                    <div class="col-md-4" style="padding:0">
                    {{ Form::text('name', '',['class' => 'form-control','rows'=>3]) }}
                    </div>
                </div>
                <div class="form-group row">
                {{ Form::label('quan', 'Quantity :',['class' => 'col-md-4','align' => 'right'])}}
                    <div class="col-md-4" style="padding:0">
                        {{ Form::text('quantity', '',['class' => 'form-control','rows'=>3]) }}
                    </div>
                </div>
                <div class="form-group row" id="description-box" style="visibility:hidden">
                {{ Form::label('description', 'Description :',['class' => 'col-md-4','align' => 'right'])}}
                    <div class="col-md-4" style="padding:0">
                        {{ Form::textarea('description', '',['class' => 'form-control','rows'=>3]) }}
                    </div>
                </div>

                <div class="form-group row" id="approve-box" style="visibility:hidden">
                    {{ Form::Label('user', 'Approved By:',['class' => 'col-md-4','align' => 'right']) }}
                    <div class="col-md-4" style="padding:0">
                        {{ Form::select('approved_by', $users->pluck('name','id'),null, array('class' => 'userSelect2 form-control'  ,'placeholder' => 'Select User'))}}
                    </div>
                </div>
                <div class="col-md-12">
                {{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
       
    </div>
</div>

@endsection
@section('js')
@parent
<script>
function myFunction() {
  var checkBox = document.getElementById("myCheck");
  if (checkBox.checked == true){
            element = document.querySelector('#user-box'); 
            element.style.visibility = 'visible'; 
            element2 = document.querySelector('#name-box'); 
            element2.style.visibility = 'hidden'; 
            element = document.querySelector('#description-box'); 
            element.style.visibility = 'hidden'; 
            element = document.querySelector('#approve-box'); 
            element.style.visibility = 'hidden'; 
  } else {
    element = document.querySelector('#name-box'); 
            element.style.visibility = 'visible'; 
            element2 = document.querySelector('#user-box'); 
            element2.style.visibility = 'hidden'; 
            element = document.querySelector('#description-box'); 
            element.style.visibility = 'visible'; 
            element = document.querySelector('#approve-box'); 
            element.style.visibility = 'visible'; 
  }
}
$(document).ready(function() {
        $(".userSelect1").select2();
    });
    $(document).ready(function() {
        $(".userSelect2").select2();
    });
</script>
@endsection
