@extends('layouts.dashboard')
@section('title')
Goody Transaction | @parent
@endsection
@section('main-content')

<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-12">
				<h1 class="page-title">User Goody Report</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="/user-report">User Goody Report</a></li>
					<li class="breadcrumb-item active">{{$user->id}}</li>
                </ol>
			</div>
			
    </div>
    
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
                    @foreach ($errors->all() as $error)
		            <div class="alert alert-danger m-1 p-2">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span>
		            </div>
                        
		              @endforeach
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <div class=" p-2 d-flex justify-center border mb-3" style="justify-content: space-between;  position: -webkit-sticky; position: sticky; top:50px; z-index: 999; background: #fff;">
    <div>
        <div class="text-muted">Person Name</div>
        <div class="font-14 font-500 text-center text-primary">{{$user->name}}</div>
    </div>
    <div>
        <div class="text-muted">Desciption</div>
        <div class="font-14 font-500 text-center">{{$user->description}}</div>
    </div>
    <div>
        <div class="text-muted">Created By</div>
        <div class="font-14 font-500 text-center">{{$user->createdBy->name}}</div>
    </div>
    <div>
        <div class="text-muted">Approved By</div>
        <div class="font-14 font-500 text-center">{{$user->approvedBy->name}}</div>
    </div>
    </div>

   
</div>

	<div class="row">
		<div class="col-md-12">
            <div class="card card-default">
                <div class="table-responsive">
                <table class="table table-striped table-outer-border no-margin table-sm" id="transaction-table">
                <thead>
                    <tr>
                        <th colspan="4"><strong>Goody Transactions</strong></th>
                    </tr>
                    <th width="10%">#</th>
                    <th width="10%">Goody Name</th>
                    <th width="15%">Quantity</th>
                    <th width="15%">Created By</th>
                    <th width="15%">Created At</th>
                </thead> 
                @if(count($userTransaction)>0)
                    @foreach($userTransaction as $index => $transaction)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text"> {{$transaction->item->name}}</td>
                            <td class="td-text"> {{abs($transaction->quantity)}}&nbsp;
                                @if($transaction->quantity < 0)
                                <span class="badge badge-success">Assigned</span>
                                @else
                                <span class="badge badge-danger">Removed</span>
                                @endif
                            </td>
                            <td class="td-text"> {{$transaction->user->name}}</td>
                            <td class="td-text"> {{datetime_in_view($transaction->created_at)}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr >
                        <td colspan="5" class="text-center"><big>No Goody Transactions added.</big></td>
                    </tr>
                @endif                  
                </table>
                </div>
            </div>
        </div>
	    </div>
    </div>


@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
		var t = $('#transaction-table').DataTable({
			fixedHeader: true,
            scrollX:        false,
            paging:         false,
            
        });
    });
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/user-report/"+optionValue;
        }
    });
    $('#selectid2').select2({
        placeholder: '{{$user ? $user->name: 'Select User'}}',
        allowClear:true
    });
    $('#goody_item_id').select2({
        placeholder: 'Select Goody',
        allowClear:true
    });
        
</script>
@endsection