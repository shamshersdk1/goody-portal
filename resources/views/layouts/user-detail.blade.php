<div class=" p-2 d-flex justify-center border mb-3" style="justify-content: space-between;  position: -webkit-sticky; position: sticky; top:50px; z-index: 999; background: #fff;">
    <div>
        <div class="text-muted">Employee Name</div>
        <div class="font-14 font-500 text-center text-primary">{{$user->name}}</div>
    </div>
    <div>
        <div class="text-muted">Employee Id</div>
        <div class="font-14 font-500 text-center">{{$user->employee_id}}</div>
    </div>
    <div>
        <div class="text-muted">Joining Date</div>
        <div class="font-14 font-500 text-center">{{date_in_view($user->joining_date)}}</div>
    </div>
    <div>
        <div class="text-muted">Confirmation Date</div>
        <div class="font-14 font-500 text-center">{{date_in_view($user->confirmation_date)}}</div>
    </div>
    <div>
        <div class="text-muted">Status
        </div>
        <div class="font-16 font-500 text-center">
            @if( $user->is_active )
                 <span class="badge badge-success" style="font-size:12px">Active</span>
            @else
                <span class="badge badge-danger" style="font-size:12px">InActive</span>
            @endif
      
       </div>
    </div>
   
</div>
