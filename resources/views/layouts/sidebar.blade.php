<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li> 
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li> 
                    <a href="/goody-items">Goody Items</a>
                </li>
                <li> 
                    <a href="/goody-purchase">Goody Purchase</a>
                </li>
                <li> 
                    <a href="/goody-assign">Assign Goody</a>
                </li>
                <li> 
                    <a href="/user-report">User Report</a>
                </li>
                <li> 
                    <a href="/goody-transaction">Goody Transaction</a>
                </li>
                <li> 
                    <a href="/goody-category">Goody Category</a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
</div>
@section('js')
@parent
<script>
    $(document).ready(function() {

        $('.fa-bars').click(function(){
            $('.sidenav').toggleClass('show');
            $('.main-container').toggleClass('slide');
        })
        $("#search").on("keyup", function () {
            if (this.value.length > 0) {   
              $("li").hide().filter(function () {
                return $(this).text().toLowerCase().indexOf($("#search").val().toLowerCase()) != -1;
              }).show(); 
            }  
            else { 
              $("li").show();
            }
        }); 

        var path = window.location.href; 
        $('ul a').each(function() {
            if (this.href === path) {
                $(this).addClass('active');
            }
        });
   });
</script> 
@endsection



