<html>
    <head>
        <link rel="stylesheet" href="{{ mix('assets/styles/dashboard.css') }}" />
        <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
        <title>@yield('title', config("app.name"))</title>
     
    </head>
    <body>
        @yield('body')

        <script src="{{ mix('assets/scripts/admin.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>

        @yield('js')
    </body>

</html>     

