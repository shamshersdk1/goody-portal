<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['session']], function () {

    //Login
    Route::get('/', 'Auth\LoginController@showLogin');
    Route::get('/logout', 'Auth\LoginController@doLogout');

    //Google Authentication Routes
    $s = 'social.';
    Route::get('/social/redirect/{provider}', ['as' => $s . 'redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => $s . 'handle', 'uses' => 'Auth\SocialController@getSocialHandle']);


    Route::get('/goody-transaction', 'GoodyTransactionController@index');
    Route::get('/user-report', 'GoodyTransactionController@userReport');
    Route::get('/user-report/{user_id}', 'GoodyTransactionController@viewReport');
    Route::post('/user-report/{user_id}', 'GoodyTransactionController@updateUserGoody');
    Route::get('goody-assign', 'GoodyAssignmentController@index');
    Route::post('goody-assign', 'GoodyAssignmentController@store');
    Route::delete('goody-assign/{id}', 'GoodyAssignmentController@destroy');
    Route::get('goody-assign/{user_id}', 'GoodyAssignmentController@viewReport');



    Route::get('/dashboard', 'DashboardController@index');

    Route::resource('goody-items','GoodyItemController');

    Route::get('/goody-purchase/{id}/approve', 'GoodyPurchaseController@updateApproval');
    Route::resource('goody-purchase','GoodyPurchaseController');
    Route::resource('goody-category','GoodyCategoryController');

});

Auth::routes();
