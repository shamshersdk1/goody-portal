<?php $__env->startSection('main-content'); ?>
    <div class="container">
        <div class="row justify-content-center xs-pt-50">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header text-center"><h4 class="xs-mb-0"><?php echo e(__('Login')); ?></h4></div>
                        <div class="card-body text-center py-4">
                                <a href="<?php echo e(route('social.redirect', ['provider' => 'google'])); ?>" class="btn btn-lg btn-danger"><i class="fa fa-google-plus" aria-hidden="true"></i> Sign in with Google</a>
                        </div>
                    </div>
            </div>
        </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('pages.session.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/shamsher/Sites/myProjects/goody-portal/resources/views/pages/auth/login.blade.php ENDPATH**/ ?>