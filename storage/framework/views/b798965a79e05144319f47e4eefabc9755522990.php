 
<?php $__env->startSection('body'); ?>



<div id="wrapper">
    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a class="logo" href="/">
                <span class="logo-light">
                    <img src="/images/logo-dark.png" style="width:140px;" alt="GeekyAnts GOODY Portal"> 
                    <span class="badge badge-primary badge-pill">GOODY</span>
                </span>
                <span class="logo-sm">
                    <img src="/images/logo-icon.png" style="width:30px" alt="GeekyAnts GOODY Portal">
                </span>
            </a>
        </div>

        <nav class="navbar-custom">
            <ul class="navbar-right list-inline float-right mb-0">
                <!-- full screen -->
                <li class="notification-list list-inline-item d-none d-md-inline-block">
                    <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                        <i class="mdi mdi-arrow-expand-all noti-icon"></i>
                    </a>
                </li>
                <li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
                    <a href="/logout" class="nav-link waves-effect">Log out</a>
                </li>

            </ul>

            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left waves-effect">
                        <i class="fa fa-bars"></i>
                    </button>
                </li>
            </ul>
        </nav>

    </div>
    <!-- Top Bar End -->
    <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
                <?php echo $__env->yieldContent('main-content'); ?>
                <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.plane', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/shamsher/Sites/myProjects/goody-portal/resources/views/layouts/dashboard.blade.php ENDPATH**/ ?>