    <div id="app">
        <?php if(!empty($errors->all())): ?>
            <div class="alert alert-danger">
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span><?php echo e($error); ?></span><br/>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php endif; ?>
        <?php if(session('message')): ?>
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span><?php echo e(session('message')); ?></span><br/>
            </div>
        <?php endif; ?>
                
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                    <img src="/images/logo-dark.png" style="width:140px;" alt="GeekyAnts Portal">
                </a>
            </div>
        </nav>
        <main class="py-4">
            <?php echo $__env->yieldContent('main-content'); ?>
        </main>
    </div>
<?php echo $__env->make('layouts.plane', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/shamsher/Sites/myProjects/goody-portal/resources/views/pages/session/app.blade.php ENDPATH**/ ?>