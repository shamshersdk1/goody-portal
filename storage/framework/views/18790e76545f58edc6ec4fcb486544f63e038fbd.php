<html>
    <head>
        <link rel="stylesheet" href="<?php echo e(mix('assets/styles/dashboard.css')); ?>" />
        <link rel="stylesheet" href="<?php echo e(mix('css/app.css')); ?>" />
        <title><?php echo $__env->yieldContent('title', config("app.name")); ?></title>
     
    </head>
    <body>
        <?php echo $__env->yieldContent('body'); ?>

        <script src="<?php echo e(mix('assets/scripts/admin.js')); ?>"></script>
        <script src="<?php echo e(mix('js/app.js')); ?>"></script>

        <?php echo $__env->yieldContent('js'); ?>
    </body>

</html>     

<?php /**PATH /Users/shamsher/Sites/myProjects/goody-portal/resources/views/layouts/plane.blade.php ENDPATH**/ ?>